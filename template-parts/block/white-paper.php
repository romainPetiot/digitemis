<?php
/**
* Block Name: Bloc white-paper
*/
?>
<section class="white-paper full-width <?php if( get_field('image')): echo 'has-image'; endif; ?>">
<?php
$slides = get_field('title');
if ( !$slides ) :
	?>
	<div style="text-align:center">
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="wrapper white-paper-wrapper">
		<div class="center white-paper-layout">
			<div class="white-paper-content">
				<h2>
					<?php the_field("title");?>
				</h2>
				<div class="baseline">
					<?php the_field("content");?>
				</div>
				<!-- Button Link -->
				<?php if(get_field('label-button') && get_field('link-button')):?>
					<a href="<?php echo get_field('link-button');?>" class="button button-brd-white">
						<?php echo get_field('label-button');?>
					</a>
				<?php endif;?>
			</div>
			<!-- Image [Optionnal] -->
			<?php
				$image = get_field('image');
				$size = 'white-paper';
				if( $image ) : ?>
					<div class="white-paper-image">
						<?php	echo wp_get_attachment_image( $image, $size ); ?>
					</div>
				<?php endif;?>
		</div>
	</div>

	<?php endif;?>
</section>
