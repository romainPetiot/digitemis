<!-- Form used for webinar and training -->
<form class="wrapper-narrow form-white-background has-section" name="formRegistration" id="formRegistration" action="#" method="POST">

    <?php $postType = get_post_type();?>
    <?php
        $terms = get_the_terms($post, 'ville');
        $ville = $terms[0];
    ?>
    <input type="hidden" name="subject" value="<?php echo esc_html($postType); ?>">
    <input type="hidden" name="title" value="<?php the_title();?>">
    <input type="hidden" name="session" value="<?php echo $ville->name;?>&#8195;—&#8195;<?php the_field("date");?>">
    
    <input type="hidden" name="honeyPot" value="">
    <div class=" half-width">
        <label for="name"><?php _e("Nom", "digitemis");?> *</label>
        <input type="text" name="name" id="name" placeholder="Garnier" required value="">
    </div>
    <div class=" half-width">
        <label for="firstname"><?php _e("Prénom", "digitemis");?> *</label>
        <input type="text" name="firstname" id="firstname" placeholder="Alain" required value="">
    </div>
    
    <div class=" full-width">
        <label for="email"><?php _e("Mail professionnel", "digitemis");?> *</label>
        <input type="email" name="email" id="email" placeholder="nom.prenom@exemple.com" required value="">
    </div>

    <div class=" full-width">
        <label for="company">Soci&eacute;t&eacute; *</label>
        <input type="text" name="company" id="company" placeholder="SARL Acme" required value="">
    </div>

    <div class=" full-width">
        <label for="activity"><?php _e("Secteur d'activité", "digitemis");?> *</label>
        <input type="text" name="activity" id="activity" placeholder="<?php _e("Communication", "digitemis");?>" value="">
    </div>


    <?php //Postal code only for training ?>
    <?php if ( $postType = 'training')  { ?>
        <div class=" full-width">
            <label for="postal"><?php _e("Code postal", "digitemis");?></label>
            <input type="text" name="postal" id="postal" placeholder="44000">
        </div>
    <?php } ?>


    <div class="full-width">
        <p><?php _e("Comment avez-vous eu connaissance de notre ", "digitemis");?><?php echo esc_html($postType->labels->singular_name); ?> ?</p>
        <div class="checkbox">
            <input type="radio" id="choice1" name="acquisition" value="Campagne email DIGITEMIS" >
            <label for="choice1"><?php _e("Campagne email DIGITEMIS", "digitemis");?></label>
        </div>
        <div class="checkbox">
            <input type="radio" id="choice2" name="acquisition" value="Mail d'invitation de votre interlocuteur privilégié DIGITEMIS" >
            <label for="choice2"><?php _e("Mail d'invitation de votre interlocuteur privilégié DIGITEMIS", "digitemis");?></label>
        </div>
        <div class="checkbox">
            <input type="radio" id="choice3" name="acquisition" value="Réseaux sociaux" >
            <label for="choice3"><?php _e("Réseaux sociaux", "digitemis");?></label>
        </div>
        <div class="checkbox">
            <input type="radio" id="choice4" name="acquisition" value="Salon ou autre évènement" >
            <label for="choice4"><?php _e("Salon ou autre évènement", "digitemis");?></label>
        </div>
        <div class="checkbox">
            <input type="radio" id="choice5" name="acquisition" value="Site web" >
            <label for="choice5"><?php _e("Site web", "digitemis");?></label>
        </div>
        <div class="checkbox">
            <input type="radio" id="choice6" name="acquisition" value="Recommandation de l'un de vos partenaires" >
            <label for="choice6"><?php _e("Recommandation de l'un de vos partenaires", "digitemis");?> </label>
        </div>
    </div>

    <p class="full-width"><?php _e("Vous n'êtes pas disponibles à ces dates ?", "digitemis");?><br><?php _e("Contactez-nous !", "digitemis");?> </p>
    
    <div id="ResponseAnchor" class="center full-width contain-button">
        <input class="button button-purple" type="submit" id="sendMessage" value="Envoyer">
    <div id="ResponseMessage">
        <?php _e("Votre message a été envoyé !", 'IHAG');?>
    </div>

    <div class="full-width center">
        <i class="formInfo">* <?php _e("Informations obligatoires", "digitemis");?></i>
        <i class="formInfo">
            <?php _e("Les informations recueillies à partir de ce formulaire sont traitées par DIGITEMIS pour donner suite à votre demande d'inscription. Pour connaître vos droits et la politique de DIGITEMIS sur la protection des données,", "digitemis"); ?>
                <a href="<?php echo get_privacy_policy_url() ?>">
                    <?php _e("Cliquez ici", "digitemis");?>
                </a>
        </i>
    </div>
</form>