<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Susty
 */

get_header();
?>

<main id="main" class="wrapper">
	<div id="page-<?php the_ID(); ?>">
		<div>

			<?php 
			if ( have_posts() ) : ?>

			<header>
				<h1 class="page-title center underline">
					<?php
					_e( "Résultat(s) pour : ", "digitemis" );
					echo '<br><b>' . get_search_query() . '</b>';
					?>
				</h1>
			</header><!-- .page-header -->

				<?php
				/* Start the Loop */

				echo "<section class='post-container'>";
				$bool = true;
					while ( have_posts() ) :
						the_post();

						if(get_post_type( get_the_ID() ) != 'post'){
							if ($bool) {
								echo "<h2 class='search-title'>";
								_e( "Page(s) correspondante(s)", "digitemis" );
								echo "</h2>";
								$bool = false;
							} 
							get_template_part( 'template-parts/content', 'search' );
						}
					endwhile;
					if ($bool) {
						echo "<h2 class='search-title'>";
						_e( "Aucune page correspondante", "digitemis" );
						echo "</h2>";
						$bool = false;
					} 
				echo "</section>";

				$show_blog_post = get_field('show_blog_post', 'option');
				if ($show_blog_post) :
					echo "<section class='post-container bloc-vertical-spacing'>";
					$bool = true;
						while ( have_posts() ) :
							the_post();

							if(get_post_type( get_the_ID() ) == 'post'){
								if ($bool) {
									echo "<h2 class='search-title'>";
									_e( "Article(s) correspondant(s)", "digitemis" );
									echo "</h2>";
									$bool = false;
								} 
								get_template_part( 'template-parts/content', 'post' );
							}
						endwhile;
						if ($bool) {
							echo "<h2 class='search-title'>";
							_e( "Aucun article correspondant", "digitemis" );
							echo "</h2>";
							$bool = false;
						} 
					echo "</section>";
				endif;

			else  : 

				echo "<header>";
				echo "<h1 class='page-title center underline'>";
				_e( "Résultat pour : ", "digitemis" );
				echo '<br><b>' . get_search_query() . '</b>';
				echo "</h1>";
				echo "</header>";

				echo "<section class='wrapper'>";
				echo "<h2 class='search-title'>";
				_e( "Aucune page correspondante", "digitemis" );
				echo "</h2>";
				echo "</section>";

				if ($show_blog_post) {
					echo "<section class='wrapper'>";
					echo "<h2 class='search-title'>";
					_e( "Aucun article correspondant", "digitemis" );
					echo "</h2>";
					echo "</section>";
				}

			endif;
			?>
		</div>
	</div><!-- #page-<?php the_ID(); ?>-->
</main><!-- #main -->

<?php
get_footer();
