<?php get_header();?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">

	<div id="page-<?php the_ID(); ?>" class="has-hero">

		<!-- Breadcrumb -->
		<div class="wrapper-narrow-container">
			<?php get_template_part( 'template-parts/content', 'hero' ); ?>
		</div>

		<!-- Title -->
		<section class="wrapper-narrow-container above-hero">
			<div class="wrapper-narrow white-bg">
				<!-- Post-title -->
				<?php the_title( '<h1 class="page-title center">', '</h1>' ); ?>
			</div>
		</section>

		<!-- Page-content -->
		<section id="post-content" class="above-hero">
			<?php the_content();?>  
		</section>

	</div><!-- #page-<?php the_ID(); ?> -->

</main><!-- #main -->

<?php endwhile; endif; ?>

<?php get_footer(); ?>