<?php
/**
 * Template Name: Training archive
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">

	<div id="page-<?php the_ID(); ?>" class="has-hero">

		<!-- Breadcrumb -->
		<div class="wrapper">
			<?php get_template_part( 'template-parts/content', 'hero' ); ?>
		</div>
		<!-- Breadcrumb -->

		<!-- Page Title -->
		<section class="wrapper above-hero">	
			<div class="sub-wrapper bg-white no-padding">
				<h1 class="page-title center">
					<?php _e("Nos formations", "digitemis");?><br>
					<span><?php _e("inter ou intra-entreprise", "digitemis");?></span>
				</h1>
			</div>
		</section>
			
		<!-- Page content -->
		<section id="sub-wrapper-content" class="above-hero">
			<?php the_content();?>
		</section>
		<!-- Page content -->

		<!--Training listing-->
		<section  class="wrapper white-bg above-hero">
			<div class="sub-wrapper">
				<?php
				global $post;
					$posts = get_posts( array(
						'post_type'			=> 'training',
						'posts_per_page' 	=> -1,
						'post_status'    	=> 'publish'
					) );
				?>

				<div class="post-container-regular">
				<?php
				if ( $posts ) {
					foreach ( $posts as $post ) :
						setup_postdata( $post ); 
						$terms = get_the_terms($post, 'ville');
						$ville = $terms[0];
						?>
						<?php get_template_part( 'template-parts/content', 'event' );?>
					<?php
					endforeach; 
					wp_reset_postdata();
				}
				?>
				</div>
			</div>
		</section>
		<!-- Training listing -->

	</div><!-- #page-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php endwhile; endif; ?>

<?php
get_footer();