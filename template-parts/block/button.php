<?php
/**
* Block Name: Bloc button
*/
?>
<section class="button-bloc bloc-vertical-spacing">
<?php
if(!get_field('label-button') || !get_field('link-button')):?>
	<div style="text-align:center">
		<span class="dashicons dashicons-admin-links"></span><br>
		<b>Bloc Bouton</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
		<a href="<?php echo get_field('link-button');?>" class="button uppercase no-margin">
			<?php echo get_field('label-button');?>
		</a>
	<?php
endif;
?>
</section>