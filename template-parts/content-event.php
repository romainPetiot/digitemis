<?php
/**
 * Template part for displaying webinar and training date in tpl-webinar-archive.php & tpl-training-archive.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */
?>

<?php
	$postType = get_post_type_object(get_post_type());
	setup_postdata( $post ); 
	$terms = get_the_terms($post, 'ville');
	$ville = $terms[0];
?>

<div class="recrutement-card webinar-card">
	<div class="recrutement-city" style="background-color:<?php the_field('bg-color', $ville);?>">
		<div class="text-bloc">
			<p class="bold xl" ><?php echo $ville->name;?></p>
			<p class="bold"><?php the_field("date");?></p>
		</div>
	</div>
	<div class="recrutement-job" style="color:<?php the_field('bg-color', $ville);?>">
		<div class="text-bloc">
			<h3 class="bold xl"><?php the_title(); ?></h3>
			<p><?php the_field("exerpt");?></p>
		</div>
		<a href="<?php the_permalink();?>" title="<?php the_title();?>" class="button no-margin button-brd-custom uppercase">
			<?php _e("En savoir plus", "digitemis");?>
		</a>
	</div>
</div><!-- <?php echo esc_html($postType->labels->singular_name);?> : <?php the_title(); ?>  -->
