<?php
/**
* Block Name: Bloc Témoignage suivant
*/
?>
<section class="synthesis next-testimonial wrapper-narrow-container full-width 
	<?php
	$blocIsWhite = false;
	if ( get_field('white-bg')  == true ) { 
		$blocIsWhite = true;
	}
	$isWhite = get_field('white-bg');
	if ( !$isWhite ) {
		echo 'blue-bg white';
	}?>
	">
<?php
$link = get_field('lien');
if ( !$link ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-format-status"></span><br>
		<b>Bloc Témoignage suivant</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="wrapper-narrow center">		
		<?php if(get_field('libelle_du_lien') && get_field('lien')):?>
			<div class="right bloc-link">
				<?php 
					$isWhite = get_field('white-bg');
					if ( $isWhite ) { ?>
						<a href="<?php echo get_field('lien');?>" class="blue right-arrow uppercase">
							<?php echo get_field('libelle_du_lien');?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/arrow-blue.svg" alt="" height="20" width="20">
					</a>
				<?php 
				} else { 
				?>
					<a href="<?php echo get_field('lien');?>" class="white right-arrow uppercase">
						<?php echo get_field('libelle_du_lien');?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/arrow-white.svg" alt="" height="20" width="20">
					</a>
				<?php 
				} ?>
				
			</div>
		<?php endif;?>
	</div>
	<?php
endif;
?>
</section>
