<?php
/**
* Block Name: Bloc values
*/
?>
<!-- Bloc based on the "cross-nav" bloc -->
<section class="cross-nav values full-width <?php echo(get_field("bg_color"))?'bloc_is_blue':'bloc_is_white';?>">
<?php
$values = get_field('values');
if ( !$values ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-shield"></span><br>
		<b>Formation : Focus Label</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
?>
	<div class="wrapper center bloc-vertical-spacing">
		
		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<h2 class="section-title"><?php the_field('title');?><br>
				<span class="section-subtitle"><?php the_field('subtitle');?></span>
			</h2>
		<?php 
		} ?>

		<div class="nav-loop">
		<?php foreach ($values as $key => $value) :
			?>
			<div class="nav-loop-item">
				<div class="way-loop-item-illustration">
						<?php
							$image = $value['image'];
							$imageColor = get_field("bg_color")?'white-img':'';
							if( $image ) {
								echo '<div class="values-img-container ';
								echo(get_field("bg_color"))?'hexagon-bg':'hexagon-bg hexagon-dark';
								echo ' ">';
                                echo wp_get_attachment_image($image, "", "", array( "class" => "$imageColor values-img" ));
                                echo '</div>';
							}
						?>
                        <h3><?php echo $value['value-name'];?></h3>
                        <p><?php echo $value['value-content'];?></p>
				</div>
			</div>
			<?php
		endforeach;?>
		</div>
	</div>
<?php endif;
?>
</section>
