<?php
/**
* Block Name: Bloc recrutement
*/
?>
<section class="recrutement-bloc wrapper">

		
		<?php
			global $post;
			$args = array(
				'post_type'			=> 'recrutement',
				'posts_per_page' 	=> -1,
				'post_status'    	=> 'publish',
				'order' => 'ASC',
				'orderby' => 'menu_order' 
			);
			$myposts = get_posts( $args );
		?>
		<div class="post-container-regular bloc-vertical-spacing">

			<!-- Loop n°1 -->
			<!-- Show all the recrutements with 'ville' (Paris, Nantes, ect…) -->
			<?php
					foreach ( $myposts as $post ) :
						setup_postdata( $post ); 
						$terms = get_the_terms($post, 'ville');
						$ville = $terms[0];
						$application = false ;
						if ( (get_field("spontanee", $ville)) == true ) {
							$application = true;
						}
						?>
						<div class="recrutement-card <?php if ( $application ) { echo "recrutement-card-application";} ?>">
							<div class="recrutement-city"
								<?php 
									if ( $application ) : 
										// Nothing happens
									else :
										// Background-color changes 
										echo 'style="background-color: '.get_field("bg-color", $ville).';" ';
									endif ;
									?>
								>
								<p class="bold xl"> 
									<?php 
									if ( $application ) :
										// Show the recrutement title 
										the_title();										
									else :
										// Show the city name
										echo $ville->name;
									endif ;?>
								</p>
							</div>
							<div class="recrutement-job" 
							<?php if ( !$application ) { 
								echo 'style="color: '.get_field("bg-color", $ville).';" ';
							} ?>>
								<h3>
									<?php 
									if ( $application ) :
										// Show the generic text for application ( ACF Field 'text-listing) 
										the_field("text-listing");										
									else :
										// Show the recrutement title 
										the_title();
									endif ;?>
								</h3>
								<a href="<?php the_permalink();?>" title="<?php the_title();?>" class="button button-brd-custom uppercase">
									<?php _e("Voir", "digitemis");?>
								</a>
							</div>
						</div>
					<?php
					endforeach; 
					wp_reset_postdata();
			?>
			<!-- Loop n°1 -->

			

		</div><!-- .post-container -->
</section>
