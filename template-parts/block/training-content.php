<?php
/**
* Block Name: Bloc training content
*/
?>
<section class="training-detail full-width">
<?php
$steps = get_field('content');
if ( !$steps ) :
	?>
	<div style="text-align:center">
	<span class="dashicons dashicons-welcome-learn-more"></span><br>
		<b>Formation : Fiche technique</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="wrapper bloc-vertical-spacing">

		<?php 
		$title = get_field( 'title' );
		if( $title ){ ?>
			<h2 class="section-title underline "><?php the_field("title");?></h2>
		<?php 
		} ?>
		
		<div class="detail-loop small-content">
		<?php foreach ($steps as $key => $step) :?>
			<h3 class="small-content-title"><?php echo $step['title'];?></h3>
			<div><?php echo $step['content'];?></div>
		<?php endforeach;?>
		</div>
	</div>
<?php endif;
?>
</section>
