<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Susty
 */

get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">
	<article id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>

		<!-- Breadcrumb -->
		<div class="wrapper-narrow-container">
			<?php get_template_part( 'template-parts/content', 'hero' ); ?>
		</div>
		
		<!-- Testimonial-title -->
		<section class="wrapper-narrow-container above-hero">
			<div class="wrapper-narrow white-bg">
				<!-- Post-title -->
				<h1 class="page-title center underline"><?php the_title();?></h1>
				<div class="entry-meta">
					<?php
					$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
					echo "<p class='author'>".esc_html( get_the_author() )."</p>" ;
					echo sprintf( $time_string,
						esc_attr( get_the_date( DATE_W3C ) ),
						esc_html( get_the_date() ),
						esc_attr( get_the_modified_date( DATE_W3C ) ),
						esc_html( get_the_modified_date() )
					);
					
					?>
				</div><!-- .entry-meta -->
			</div>
		</section>
		
		<div id="post-content" class="above-hero">
			<?php if(is_single()):?>
				<?php the_content();?>
			<?php else:?>
				<?php the_excerpt();?>
			<?php endif;?>
			<section id="post-share" class="wrapper-narrow">
				<p><?php _e("Je partage", "digitemis")?></p>

				<a class="JSrslink" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo nbTinyURL(get_the_permalink());?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/facebook.svg" height="24" width="24">
				</a>
				<a class="JSrslink" href="https://www.linkedin.com/cws/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin.svg" height="24" width="24">
				</a>
				<a class="JSrslink" href="https://www.twitter.com/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter.svg" height="24" width="24">
				</a>

			</section>
		</div><!-- #post-content -->

		<section  class="wrapper bloc-vertical-spacing above-hero">
			<h2 class="section-title center underline"><?php _e("Derniers articles", 'digitemis');?></h2>
			<?php
			global $post;
				$lastposts = get_posts( array(
					'posts_per_page' => 2,
					'post_status'    => 'publish'
				) );
			?>
			<div class="post-container">
				<?php
					if ( $lastposts ) {
						foreach ( $lastposts as $post ) :?>

						<?php get_template_part( 'template-parts/content', 'post' );?>

						<?php
						endforeach; 
						wp_reset_postdata();
					}
				?>
			</div><!-- .post-container -->
		</section>
	</article><!-- #post-<?php the_ID(); ?> -->
</main><!-- #main -->

<?php endwhile; endif; ?>

<?php
get_footer();
