<?php
/**
* Block Name: Bloc Chapo
*/
?>
<section class="chapo 
	<?php
	$animation = get_field('animation');
	if ( $animation ) {
		echo 'animation';
	}
	?>
	">
<?php
$chapo = get_field('title');
if ( !$chapo ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-editor-paragraph"></span><br>
		<b>Bloc Chapô</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="wrapper bloc-vertical-spacing center">
		<h1>
			<?php the_field('title');?><br>
			<span class="font-cursive regular"><?php the_field('sub-title');?></span>
		</h1>
		<?php the_field('content');?>
	</div>
	<?php
endif;
?>
</section>
