<?php
/**
* Block Name: Bloc Légende (notre démarche)
*/
?>
<section class="legend">
<?php
$text = get_field('text');
if ( !$text ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-editor-paragraph"></span><br>
		<b>Bloc Légende</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :
	?>
	<div class="legend-layout wrapper">
		<div class="legend-text entry-content white blue-bg"><?php the_field('text');?></div>
		<div class="legend-link ">
			<a href="<?php echo get_field('link');?>" class="button uppercase no-margin">
				<?php echo get_field('label');?>
			</a>
		</div>
		
	</div>
	<?php
endif;
?>
</section>
