<?php
/**
* Block Name: Bloc Header Image
*/
?>
<section class="header-image has-element full-width">
<?php
$content = get_field('content');
if ( !$content ) :
	?>
	<div style="text-align:center">
		<span class="dashicons dashicons-format-image"></span><br>
		<b>Header Image</b><br>
		<em>Renseigner les informations</em>
	</div>
	<?php
else :?>
	<div class="bloc-background white" style="background: center / cover no-repeat url(<?php echo get_field('bg-image');?>)">
		<div class="wrapper layout has-element">
			<div class="entry-content big-f-size">				
				<?php the_field('content');?>
			</div>
			<div class="picture">				
				<?php
					$image = get_field('image');
					$size = 'free-height';
					if( $image ) {
						echo wp_get_attachment_image( $image, $size );
					}
				?>
			</div>
		</div>
	</div>
		
<?php endif; ?>
</section>
