<?php
/**
 * Template Name: Archive blog - Ne pas utiliser
 *
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">
    <article id="page-<?php the_ID(); ?>" <?php post_class('has-hero'); ?>>

        <!-- Breadcrumb -->
        <?php get_template_part( 'template-parts/content', 'hero' ); ?>

        <!-- Page content -->
        <section id="gutenberg-content" class="above-hero white-bg">
            <?php the_content();?>  
        </section>
        
        <?php endwhile; endif;?>
        <?php wp_reset_postdata(); ?>
        <!-- Page Content -->

        <!-- Posts Content -->
        <section  class="wrapper white-bg above-hero">
            <?php if ( have_posts() ) : ?>

                <?php $terms = get_terms( array('taxonomy' => 'category',) );?>

                <!-- For developpement purpose - cand be deleted later -->

                <nav id="nav-category">
                    <ul>
                    <?php foreach ( $terms as $term ) :?>
                        <li class="js-change-category" data-categoy="<?php echo $term->term_id;?>">
                            <a href="<?php echo get_term_link($term);?>">
                                <?php echo '<h2>'.$term->name . '</h2>';?>
                            </a>
                        </li>
                    <?php endforeach;?>
                    <?php wp_reset_postdata(); ?>
                    </ul>
                </nav><!-- #nav-category -->

                <?php
                global $post;
                $args = array( 
                        'posts_per_page'   => -1,
                        'post_type'        => 'post',
                        'post_status'      => 'publish'
                    );
                $myposts = get_posts( $args ); ?>

                <div class="post-container">

                    <?php
                    foreach ( $myposts as $post ) : 
                    ?>
                    <?php get_template_part( 'template-parts/content', 'post' );?>
                <?php endforeach; ?>
                <?php the_posts_navigation(); ?>
                </div><!-- .post-container -->
                
            <?php else :?>
                
                <?php echo "<p class='center'>Aucun article disponible</p>"; ?>
                <?php get_template_part( 'template-parts/content', 'none' );?>
                    
            <?php endif;?>
            <?php wp_reset_postdata(); ?>
        </section><!-- Posts Content -->

    </article><!-- #page-<?php the_ID(); ?> -->
</main>

<?php get_footer(); ?>