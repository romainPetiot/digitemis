<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Susty
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16px.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-96px.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32px.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" sizes="124x124">
	<link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" />
	<meta name="theme-color" content="#144660">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'susty' ); ?></a>
	<a class="skip-link screen-reader-text" href="#menu"><?php esc_html_e( 'Menu', 'susty' ); ?></a>
	<header id="masthead">
		<div id="topbar-wrapper">
			<div id="topbar-logo">
				<div id="topbar-logo-content">
					<a href="<?php echo get_home_url(); ?>">
						<?php $image = get_field('logo_header', 'option');
							$size = '200';
							if( $image ) {
							echo wp_get_attachment_image( $image, $size );
						}?>
					</a>
				</div>
			</div>
			<div role="button" id="link-menu" class="desktop-hidden tablet-hidden reset">
				<span id="burger-menu-1" aria-hidden="true"></span>
				<span id="burger-menu-2" aria-hidden="true"></span>
				<span id="burger-menu-3" aria-hidden="true"></span>
			</div>
			<div id="menu">
				<div id="menu-translate">
					<?php echo ihag_menu('translation'); ?>
				</div>
				<div id="menu-mobile-container">
					<div id="menu-primary">
						<?php echo ihag_menu('primary'); ?>

						<?php // Contact 
						$btn_contact = get_field('btn_contact', 'option');
						if ($btn_contact) {
							?>
							<div id="btn-contact">
								<a class="button" href="<?php echo $btn_contact ?>"><?php esc_html_e( 'Contact', 'digitemis' ); ?></a>
							</div>
							<?php
						}?>

					</div>
					<div id="menu-secondary">
						<?php echo ihag_menu('secondary'); ?>
					</div>
				</div>
				<div id="topbar-social" class="social-icon tiny-hidden mobile-hidden">
					<?php
						if( get_field('linkedin', 'option') ): ?>
							<a href="<?php the_field('linkedin', 'option'); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin-white.svg"  calt="Linkedin" height="20" width="21">
							</a>
					<?php endif;
					if( get_field('facebook', 'option') ): ?>
						<a href="<?php the_field('facebook', 'option'); ?>">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/facebook-white.svg"  calt="Facebook" height="20" width="9">
						</a>
					<?php endif;
					if( get_field('twitter', 'option') ): ?>
						<a href="<?php the_field('twitter', 'option'); ?>">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter-white.svg"  calt="Twitter" height="20" width="24">
						</a>
					<?php endif;
					if( get_field('youtube', 'option') ): ?>
						<a href="<?php the_field('youtube', 'option'); ?>">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/youtube-white.svg"  calt="Youtube" height="20" width="29">
						</a>
					<?php endif; ?>
				</div>
				<div id="menu-search">
					<input id="icon-search" onclick="toggleSearchbar()" type="image" alt="Search" src="<?php echo get_stylesheet_directory_uri(); ?>/image/search.svg" height="20" width="20" />				
					<form id="form-search" action="<?php echo home_url();?>" method="get" class="white-menu">
						<div id="form-wrapper" class="wrapper" >
							<input type="text" name="s" id="search" class="light-grey-bg" placeholder="<?php _e("Formation, Audit de conformité, Phishing…", "digitemis");?>" value="<?php the_search_query(); ?>" />
							<input type="submit" id="send-search" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" class="button"/>
						</div>
					</form>
				</div>
			</div>
		</div>

		<style>
			
			#menu-primary > ul > li.services-nav-style > ul .link-has-thumbnail > a::after {
			background-image: url('<?php if( get_field('notreDemarche', 'option') ): the_field('notreDemarche', 'option'); endif; ?>');
			background-size: cover;
			background-position: center;
			}
		}
		</style>
	</header>
	<div id="topbar-background" aria-hidden="true" ></div>

	<div id="content">
