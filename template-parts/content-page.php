<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

?>

<div id="page-<?php the_ID(); ?>" <?php if ( !is_front_page() ) { post_class('has-hero'); } ?>>

	<?php if ( !is_front_page() ){
		get_template_part( 'template-parts/content', 'hero' );
	} ?>

	<!-- Page-content -->
	<div id="gutenberg-content" class="above-hero">
		<?php the_content();?>
	</div>

</div><!-- #page-<?php the_ID(); ?> -->
