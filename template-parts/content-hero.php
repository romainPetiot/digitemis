<?php if ( has_post_thumbnail() ) : ?>
    <style> 
        #page-<?php the_ID();?>::after {
            <?php if (wp_is_mobile()) : ?>
				background-image: url(<?php the_post_thumbnail_url('page-hero-mobile'); ?>);
			<?php else : ?>
				background-image: url(<?php the_post_thumbnail_url('page-hero'); ?>);
			<?php endif; ?>
        }
    </style>
<?php endif; ?>

<?php        $allow_breadcrumb = get_field('allow_breadcrumb', 'option');
        if ($allow_breadcrumb) {
            wpBreadcrumb(); 
        }?>