<?php
/**
 * Template Name: Livre blanc
 * 
 */
get_header();
?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<main id="main">
	
	<div id="page-<?php the_ID(); ?>" class="has-hero">

		<!-- Breadcrumb -->
		<div class="wrapper-narrow-container">
			<?php get_template_part( 'template-parts/content', 'hero' ); ?>
		</div>
		
		
		<!-- Content -->
		<section class="above-hero">
			<div class="wrapper-narrow-container">
				<div class="wrapper-narrow white-bg">
					<!-- Page-title -->
					<h1 class="page-title center"><?php the_title();?></h1>
				</div>
			</div>
		</section>

		<!-- Page-content -->
		<div id="post-content" class="above-hero">
			<?php the_content();?>
		</div>

		<!-- Form 
		<section  class="wrapper-narrow-container narrow-form above-hero">
			<form class="form-white-background has-section" name="contactLivreBlanc" id="contactLivreBlanc" action="#" method="POST">

			<input type="hidden" name="honeyPot" value="">
				<div class=" half-width">
					<label for="name"><?php // _e("Nom", "digitemis");?> *</label>
					<input type="text" name="name" id="name" placeholder="Garnier" required value="">
				</div>
				<div class="half-width">
					<label for="firstname"><?php // _e("Prénom", "digitemis");?> *</label>
					<input type="text" name="firstname" id="firstname" placeholder="Alain" required value="">
				</div>
				
				<div class="half-width">
					<label for="email"><?php // _e("Mail", "digitemis");?> *</label>
					<input type="email" name="email" id="email" placeholder="nom.prenom@exemple.com" required value="">
				</div>
				
				<div class="half-width">
					<label for="phone"><?php // _e("Téléphone", "digitemis");?> *</label>
					<input type="tel" name="phone" id="phone" placeholder="06 XX XX XX XX" required >
				</div>

				<div class="full-width">
					<label for="company"><?php // _e("Société", "digitemis");?> *</label>
					<input type="text" name="company" id="company" placeholder="SARL Acme" required value="">
				</div>

				<div class="full-width">
					<label for="activity"><?php // _e("Secteur d'activité", "digitemis");?></label>
					<input type="text" name="activity" id="activity" placeholder="<?php // _e("Communication", "digitemis");?>" value="">
				</div>

				<div class="full-width">
					<label for="function"><?php // _e("Fonction", "digitemis");?> *</label>
					<input type="text" name="function" id="function" placeholder="<?php // _e("Responsable technique", "digitemis");?>" required value="">
				</div>

				<div class="checkbox no-margin full-width">
					<input type="checkbox" name="NewsletterCheckbox" id="NewsletterCheckbox" value="askNewsletter">
					<label for="NewsletterCheckbox"><?php // _e("Je ne souhaite pas recevoir les newsletter de DIGITEMIS", "digitemis");?></label>
				</div>

				<div class="checkbox no-margin full-width">
					<input type="checkbox" name="recontactCheckbox" id="recontactCheckbox">
					<label for="recontactCheckbox"><?php // _e("Je ne souhaite pas être recontacté(e) par DIGITEMIS", "digitemis");?></label>
				</div>

				<p class="full-width"><?php // _e("Si vous ne recevez pas d'e-mail après confirmation de l'envoi, veuillez vérifier dans votre dossier Courrier Indésirable.", "digitemis");?></p>


				<div id="ResponseAnchor" class="center full-width contain-button">
					<input class="button button-purple" type="submit" id="sendMessage" value="Envoyer">
				<div id="ResponseMessage">
					<?php // _e("Livre blanc envoyé ! Vous allez recevoir un e-mail à l'adresse que vous avez indiquée dans le formulaire.", 'digitemis');?>
				</div>

				<div class="full-width center">
					<i class="formInfo">* <?php // _e("Informations obligatoires", "digitemis");?></i>
					<i class="formInfo"><?php // _e("Les informations recueillies à partir de ce formulaire sont traitées par DIGITEMIS pour donner suite à votre demande de téléchargement de livre blanc et dans le cadre de ses actions de prospection commerciale. Pour connaître vos droits et la politique de DIGITEMIS sur la protection des données,", "digitemis");?> 
						<a href="<?php // echo get_privacy_policy_url() ?>">
							<?php // _e("Cliquez-ici", "digitemis");?>.
						</a>
					</i>
				</div>
			</form>
		</section>
		-->

	</div><!-- #page-<?php the_ID(); ?> -->

</main><!-- #main -->

<?php endwhile; endif; ?>

<?php get_footer(); ?>